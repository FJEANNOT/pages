# Formation Beetobee - Déployez votre site avec Gitlab CI!

## Construire une pipeline

Pour construire une pipeline avec Gitlab CI, il faut tout d'abord créer un fichier `.gitlab-ci.yml`

Ce fichier nous permet de déclarer différentes instructions à éxécuter lors de chaque push.

Habituellement, on a des étapes de build, de test et de déploiement.

On utilise le mot clé `stages` définir ces étapes:

```yaml
stages:
  - test
  - build
  - deploy
```

Ensuite, on peut définir autant de *jobs* éxécutés en concurrence avec le nom de votre choix.

On rattache le job à une étape avec le mot clé `stage` (au singulier).

```yaml
docker-build:
  stage: build
```

Quand on utilise un runner *docker* ou *kubernetes*, on précise un nom d'image avec le mot clé `image`

```yaml
docker-build:
  stage: build
  image: ubuntu:latest
```

Enfin, pour faire fonctionner le *job*, on ajoute le mot clé `script` pour définir les commandes à éxécuter.

```yaml
docker-build:
  stage: build
  image: ubuntu:latest
  script:
  - docker build .
```

